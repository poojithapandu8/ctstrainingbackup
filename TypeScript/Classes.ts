class Person {
    //private readonly name: string; 
    public constructor(private name: string) {}
  
    public getName(): string {
      return this.name;
    }
  }
        
  const person = new Person("Jane");
  console.log(person.getName()); 


  interface Car {
    make: string;
    model: string;
    mileage?: number;
  }
              
  let myCar: Required<Car> = {
    make: 'Ford',
    model: 'Focus',
    mileage: 12000 // `Required` forces mileage to be defined
  };
  console.log(myCar);

  interface Point {
    x: number;
    y: number;
  }
              
  let pointPart: Partial<Point> = {}; // `Partial` allows x and y to be optional
  pointPart.x = 10;
  console.log(pointPart);