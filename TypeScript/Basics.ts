var message:string="Welcome to Typescript"; //explicit varaible declaration
console.log(message);

let a="Welcome to session";//implcit variable declaration
console.log(a);

var m:any="Testing";
//m=2;
console.log(m);

console.log((<string>m).length) //type castign to string from any datatype

function printStatusCode(code: string | number) { //union type oeprator input will be either string or number
    console.log(`My status code is ${code}.`)
  }
  
  printStatusCode(404);
  printStatusCode('404');