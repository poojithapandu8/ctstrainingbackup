function get(): number { //no paramters
    return 3+5;
  }
  console.log(get());

  function multiply(a: number, b: number) { //paramters
    return a * b;
  } 
  console.log(multiply(2,5))

  function add(a: number, b: number, c?: number) { //optional parameter
    return a + b + (c || 0);
  }
  
  console.log(add(2,5))