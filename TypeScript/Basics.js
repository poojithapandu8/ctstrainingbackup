var message = "Welcome to Typescript"; //explicit varaible declaration
console.log(message);
var a = "Welcome to session"; //implcit variable declaration
console.log(a);
var m = "Testing";
//m=2;
console.log(m);
console.log(m.length); //type castign to string from any datatype
function printStatusCode(code) {
    console.log("My status code is ".concat(code, "."));
}
printStatusCode(404);
printStatusCode('404');
