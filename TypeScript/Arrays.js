var names = ["1", "2", "3"];
names.push("4");
console.log(names);
var c; //tuple fixes length,fixed data type
c = ["Poojitha", 24];
console.log(c);
var car = {
    type: "Toyota",
    model: "Corolla",
    year: 2009
}; //object type
console.log(car);
console.log(car.year);
