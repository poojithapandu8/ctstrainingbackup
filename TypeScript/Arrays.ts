var names:string[]=["1","2","3"];
names.push("4");
console.log(names);

let c:[string,number]; //tuple fixes length,fixed data type
c=["Poojitha",24];
console.log(c);

const car: { type: string, model: string, year: number } = {
    type: "Toyota",
    model: "Corolla",
    year: 2009
  }; //object type
  
  console.log(car);
  console.log(car.year);