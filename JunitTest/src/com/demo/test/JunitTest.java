package com.demo.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Ignore;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Test;

//@TestInstance(value = null)
class JunitTest {
	
    @Disabled
	@Test
	void test() {
		fail("Not yet implemented");
	}
	
    @Tag("dev-test")
    @Test
     void addCalculator() {
    	System.out.println("Executing First TestCase");
    	Assertions.assertEquals(9,Calculator.add(5,4));
    }
	
    
    @Test
    void addCalculator1() {
   	System.out.println("Executing First TestCase");
   	Assertions.assertEquals(9,Calculator.add(5,4));
   }
	
	
	@BeforeAll()
	static void setup4() {
		System.out.println("Executing the BeforeAll block code");
	}
	
	@BeforeEach()
	 void setup1() {
		System.out.println("Executing the BeforeEach block code");
	}
	
	@AfterEach()
	 void setup2() {
		System.out.println("Executing the AfterEach block code");
	}
	
	@AfterAll()
	static void setup3() {
		System.out.println("Executing the AfterAll block code");
}

}
