package com.demo.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.demo.model.Book;
import com.test.Service.BookService;

class DemoAssertTrue {
	BookService bookService=new BookService();
	
	@Test
	void test() {		
		assertTrue(bookService.getBooks().isEmpty());
	}
	
	@Test
	void test1() {
		bookService.addBooks(new Book("1","book name","book publisher"));
		//System.out.println(bookService.getBooks());
		assertFalse(bookService.getBooks().isEmpty(),"Book is found"+bookService.getBooks());
	}


}
