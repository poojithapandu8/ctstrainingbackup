package com.SBJunit.demo.Controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.SBJunit.demo.Entity.Person;
import com.SBJunit.demo.Repository.PersonRepository;

@SpringBootTest
class MyControllerTest {
   @Autowired
   private PersonRepository personRepository;
   
	@Test
	void test() {
		Person person=new Person(2,"pooji k");
		personRepository.save(person);
		//fail("Not yet implemented");
		assertTrue(personRepository.isPersonExistsById(2));
	}
	

	@BeforeAll()
	static void setup4() {
		System.out.println("Executing the BeforeAll block code");
	}
	
	@BeforeEach()
	 void setup1() {
		System.out.println("Executing the BeforeEach block code");
	}
	
	@AfterEach()
	 void setup2() {
		System.out.println("Executing the AfterEach block code");
		personRepository.deleteAll();
	}
	
	@AfterAll()
	static void setup3() {
		System.out.println("Executing the AfterAll block code");
}


}
