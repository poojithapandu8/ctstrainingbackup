package com.SBJunit.demo.Controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.SBJunit.demo.Repository.PersonRepository;
import com.SBJunit.demo.Service.PersonService;

@ExtendWith(MockitoExtension.class)
class MockitoTest {
	
	@Mock
	private PersonRepository personRepository;
	private PersonService personService;
	
	@BeforeEach
	void steup() {
		this.personService=new PersonService(this.personRepository);
	}

	@Test
	void test() {
		personService.getAllPersons();
		verify(personRepository).findAll();
		
	}

}
