package com.SBJunit.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.SBJunit.demo.Service.PersonService;

@RestController
public class MyController {
	
	@Autowired
	private PersonService personService;
	
	@GetMapping("/person")
	/*void welcome() {
		System.out.println("Welcome!!");
	}*/
	ResponseEntity<?> getAllPerson(){
		System.out.println("Printing in the Controller block");
		return ResponseEntity.ok(this.personService.getAllPersons());
	}

}
