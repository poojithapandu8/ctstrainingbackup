package com.SBJunit.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.SBJunit.demo.Entity.Person;

public interface PersonRepository extends JpaRepository <Person,Integer>{
 @Query("SELECT CASE WHEN COUNT(S) >0 THEN TRUE ELSE FALSE END FROM Person S WHERE S.personID=?1")
 Boolean isPersonExistsById(Integer id);
}
