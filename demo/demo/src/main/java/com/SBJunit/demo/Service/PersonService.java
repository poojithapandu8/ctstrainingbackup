package com.SBJunit.demo.Service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.SBJunit.demo.Entity.Person;
import com.SBJunit.demo.Repository.PersonRepository;

@Service
public class PersonService {
    @Autowired
	private PersonRepository personRepository;

	public PersonService(PersonRepository personRepository) {
		super();
		this.personRepository = personRepository;
	}
    
	public List<Person> getAllPersons(){
		return this.personRepository.findAll();
	}

}
