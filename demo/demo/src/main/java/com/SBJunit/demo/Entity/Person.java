package com.SBJunit.demo.Entity;

import javax.persistence.Entity;
import javax.persistence.*;

@Entity
public class Person {
	
	@Id
	private Integer personID;
	private String personName;
	public Integer getPersonID() {
		return personID;
	}
	public void setPersonID(Integer personID) {
		this.personID = personID;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public Person(Integer personID, String personName) {
		super();
		this.personID = personID;
		this.personName = personName;
	}
	public Person() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Person [personID=" + personID + ", personName=" + personName + "]";
	}
	
	
	

}
