package CTSTraining;

import java.util.*;

public class ForEach {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
   List<Integer> data=new ArrayList<Integer>();
   data.add(0);data.add(01);data.add(02);data.add(03);data.add(04);
   
   Iterator i=data.iterator();
   System.out.println("Iterating over elements using Iterator");
   while(i.hasNext()) {
	   System.out.println(i.next());
   }
   System.out.println("Iterating over elements using forEach");
   data.forEach((a)->{System.out.println(a);});
   data.forEach(System.out::println);
   
   
	}
}
