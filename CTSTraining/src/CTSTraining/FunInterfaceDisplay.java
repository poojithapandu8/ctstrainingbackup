package CTSTraining;

public class FunInterfaceDisplay {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
     FunInterface fInterface=new FunInterface() {

		@Override
		public void display() {
			// TODO Auto-generated method stub
			System.out.println("Overriden single abstract method - Functional Interface");
		}
    	 
     };
     fInterface.display();
     fInterface.welcome();
     
  // (new FunInterface() {()->System.out.println("Overriden single abstract method - Functional Interface")};).display();
	}

}
