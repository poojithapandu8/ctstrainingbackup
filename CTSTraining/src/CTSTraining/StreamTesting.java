package CTSTraining;

import java.util.*;
import java.util.stream.Collectors;

public class StreamTesting {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> data=new ArrayList<Integer>();
		   data.add(0);data.add(01);data.add(02);data.add(03);data.add(04);
		   
		   System.out.println("Collecting to List "+data.stream().filter(a->a<11).map(k->k+10).collect(Collectors.toList()));
		   System.out.println("Parallel Stream --");
		   data.parallelStream().filter(a->a>1).forEach(System.out::println);
		   System.out.println("Stream --");
		   data.stream().filter(a->a>1).forEach(System.out::println);
		   System.out.println("Counting elements "+data.stream().filter(a->a>2).count());
			  
	}

}
