package CTSTraining;

import java.util.*;

public class OptionalTesting {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Optional op=Optional.of(3);
		System.out.println(op.get());
		System.out.println(op.isPresent());
		System.out.println(Optional.empty());
		System.out.println(Optional.ofNullable(null));
		//error System.out.println(Optional.of(null));
		op.ifPresent((a)->{System.out.println(a+" value is not null.");});
	}
}
